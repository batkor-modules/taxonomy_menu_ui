<?php

namespace Drupal\taxonomy_menu_ui\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides menu links deriver.
 */
class TaxonomyMenuUiLinks extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The token system.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * The term storage.
   *
   * @var \Drupal\taxonomy\TermStorage
   */
  protected $termStorage;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    $instance = new static();
    $instance->configFactory = $container->get('config.factory');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->token = $container->get('token');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $config = $this
      ->configFactory
      ->get('taxonomy_menu_ui.settings');
    $menu_list = $config->get('menu_list') ?? [];
    foreach ($menu_list as $vid => $settings) {
      $this->buildLink($vid, $settings, $base_plugin_definition);

    }

    return $this->derivatives;
  }

  /**
   * Recursive adding derivative menu.
   *
   * @param string $vid
   *   The vocabulary id.
   * @param array $settings
   *   The menu settings.
   * @param array $base_plugin_definition
   *   The definition array of the base plugin.
   * @param int $term_parent
   *   The taxonomy parent id.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function buildLink($vid, array $settings, array $base_plugin_definition, $term_parent = 0) {
    /** @var \Drupal\taxonomy\Entity\Term[] $terms */
    $terms = $this
      ->getTermStorage()
      ->loadTree($vid, $term_parent, 1, TRUE);

    foreach ($terms as $term) {
      // Set menu item title.
      if (isset($settings['link_default']['title']) && $settings['link_default']['title']) {
        $title = $this->token->replace($settings['link_default']['title'], ['term' => $term], ['clear' => TRUE]);
      }
      else {
        $title = $term->getName();
      }
      // Set description.
      $description = '';
      if (isset($settings['link_default']['description']) && $settings['link_default']['description']) {
        $description = $this->token->replace($settings['link_default']['description'], ['term' => $term], ['clear' => TRUE]);
      }
      if ($term_parent === 0) {
        [$menu_name, $menu_item_parent] = explode(':', $settings['menu_parent'], 2);
      }
      else {
        $menu_name = $settings['menu_name'];
        $menu_item_parent = 'taxonomy_menu_ui.links:' . $term_parent;
      }
      // Set expanded value.
      $expanded = $settings['link_default']['expanded'] ?? FALSE;
      $this->derivatives[$term->id()] = [
        'title' => $title,
        'description' => $description,
        'menu_name' => $menu_name,
        'parent' => $menu_item_parent,
        'weight' => $term->getWeight(),
        'expanded' => $expanded,
        'route_name' => $term->toUrl()->getRouteName(),
        'route_parameters' => ['taxonomy_term' => $term->id()],
      ] + $base_plugin_definition;
      // Build links for child's.
      $this->buildLink($vid, $settings, $base_plugin_definition, $term->id());
    }
  }

  /**
   * Returns term storage.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface|\Drupal\taxonomy\TermStorage|mixed|object
   *   This term storage.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getTermStorage() {
    if ($this->termStorage) {
      return $this->termStorage;
    }
    $this->termStorage = $this
      ->entityTypeManager
      ->getStorage('taxonomy_term');
    return $this->termStorage;
  }

}
