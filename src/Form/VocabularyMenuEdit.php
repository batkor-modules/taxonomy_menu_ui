<?php

namespace Drupal\taxonomy_menu_ui\Form;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure taxonomy_menu_ui settings for this site.
 */
class VocabularyMenuEdit extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The token system.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * The parent form selector service.
   *
   * @var \Drupal\Core\Menu\MenuParentFormSelectorInterface
   */
  protected $menuParentSelector;

  /**
   * The menu link manager.
   *
   * @var \Drupal\Core\Menu\MenuLinkManager
   */
  protected $menuLinkManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new static($container->get('config.factory'));
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->token = $container->get('token');
    $instance->menuParentSelector = $container->get('menu.parent_form_selector');
    $instance->menuLinkManager = $container->get('plugin.manager.menu.link');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'taxonomy_menu_ui_vocabulary_menu_edit';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['taxonomy_menu_ui.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $taxonomy_vocabulary = NULL) {

    $config = $this->config('taxonomy_menu_ui.settings');
    $form_state->set('vid', $taxonomy_vocabulary);

    $current_settings = $config->get('menu_list.' . $taxonomy_vocabulary);

    $form['menu'] = [
      '#type' => 'details',
      '#title' => $this->t('Menu settings'),
      '#attached' => [
        'library' => ['menu_ui/drupal.menu_ui.admin'],
      ],
      '#group' => 'additional_settings',
      '#open' => $current_settings['menu_name'] ? TRUE : FALSE,
    ];

    $form['menu']['menu_name'] = [
      '#type' => 'radios',
      '#title' => $this->t('Available menus'),
      '#options' => menu_ui_get_menus(),
      '#default_value' => $current_settings['menu_name'],
      '#description' => $this->t('The menus available to place links in for this content type.'),
    ];

    // @todo See if we can avoid pre-loading all options by changing the form or
    //   using a #process callback. https://www.drupal.org/node/2310319
    //   To avoid an 'illegal option' error after saving the form we have to
    //   load all available menu parents. Otherwise, it is not possible to
    //   dynamically add options to the list using ajax.
    $options_cacheability = new CacheableMetadata();
    $options = $this
      ->menuParentSelector
      ->getParentSelectOptions('', NULL, $options_cacheability);
    $form['menu']['menu_parent'] = [
      '#type' => 'select',
      '#title' => $this->t('Default parent item'),
      '#default_value' => $current_settings['menu_parent'],
      '#options' => $options,
      '#description' => $this->t('Choose the menu item to be the default parent for a new link in the content authoring form.'),
      '#attributes' => ['class' => ['menu-title-select']],
    ];
    $options_cacheability->applyTo($form['menu']['menu_parent']);

    $form['menu']['token_tree'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => [
        'site',
        'term',
      ],
      '#show_restricted' => TRUE,
      '#global_types' => FALSE,
    ];

    $form['menu']['link']['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default title for menu item'),
      '#description' => $this->t('Leave blank for get title from term name.'),
      '#default_value' => $current_settings['link_default']['title'] ?? '',
    ];

    $form['menu']['link']['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Default description for menu item'),
      '#rows' => 1,
      '#description' => $this->t('Shown when hovering over the menu link.'),
      '#default_value' => $current_settings['link_default']['description'] ?? '',
    ];

    $form['menu']['link']['expanded'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Expanded'),
      '#description' => $this->t('Show every menu links expanded.'),
      '#default_value' => $current_settings['link_default']['expanded'] ?? '',
    ];

    $form['actions']['disable_generate'] = [
      '#type' => 'submit',
      '#value' => $this->t('Disable generate'),
      '#suffix' => '<i>' . $this->t('Remove menu items created from term list current vocabulary.') . '</i>',
      '#weight' => 500,
      '#submit' => ['::disableMenuItems'],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Clear settings and rebuild menu links.
   *
   * @param array $form
   *   The renderable form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function disableMenuItems(array &$form, FormStateInterface $form_state) {
    $this->config('taxonomy_menu_ui.settings')
      ->clear("menu_list.{$form_state->get('vid')}")
      ->save();
    $this->menuLinkManager->rebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $link_default = [
      'title' => $form_state->getValue('title'),
      'description' => $form_state->getValue('description'),
      'expanded' => $form_state->getValue('expanded'),
    ];
    $vid = $form_state->get('vid');
    $this->config('taxonomy_menu_ui.settings')
      ->set("menu_list.{$vid}.menu_name", $form_state->getValue('menu_name'))
      ->set("menu_list.{$vid}.menu_parent", $form_state->getValue('menu_parent'))
      ->set("menu_list.{$vid}.individ_settings", $form_state->getValue('individ_settings'))
      ->set("menu_list.{$vid}.link_default", $link_default)
      ->save();
    $this->menuLinkManager->rebuild();
    parent::submitForm($form, $form_state);
  }

}
