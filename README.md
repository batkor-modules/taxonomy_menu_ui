# Taxonomy Term UI (Fork)

This module fork  main module by Drupal.org.
[Taxonomy Menu UI](https://www.drupal.org/project/taxonomy_menu_ui/git-instructions)

## Introduction
This module provides interface for fast generate menu links from vocabularies.

## Installation
1. `git clone https://gitlab.com/batkor-modules/taxonomy_menu_ui.git modules/custom/taxonomy_menu_ui`;
2. Enable module;
3. Goto `/admin/structure/taxonomy/manage/{you_vocabulary}/menu_edit`;
4. Set your settings and save form.
